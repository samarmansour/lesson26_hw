﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson26_HW
{
    class MyProtectedUniqueList
    {

        private List<string> words;
        private string word;

        public MyProtectedUniqueList(string word)
        {
            words = new List<string>();
            this.word = word; 

        }

        public void Add(string word)
        {
            if (word == null || word == " ")
            {
                throw new ArgumentNullException("string is empty/null");
            }

            if (words.Contains(word))
                throw new InvalidOperationException($"{word} already exists!");

            words.Add(word);
        }

        public void Remove(string word)
        {
            if (word == null || word == " ")
            {
                throw new ArgumentNullException("string is empty/null");
            }
            if (!words.Contains(word))
            {
                throw new ArgumentException($"{word} not exists!");
            }
            words.Remove(word);
        }

        public void RemoveAt(int wordIndex)
        {
            if (wordIndex < 0)
            {
                throw new ArgumentOutOfRangeException("Index Cannot be negative!");
            }
            if (wordIndex > words.Count)
            {
                throw new ArgumentOutOfRangeException("Index Out of list range");
            }
            words.RemoveAt(wordIndex);
        }

        public void Clear(string word)
        {
            if (this.word != word)
            {
                throw new AccessViolationException("Words are not matching");
            }
            else
            {
                words.Clear();
            }
        }

        public void Sort(string word)
        {

            if (this.word != word)
            {
                throw new AccessViolationException("Words are not matching");
            }
            else
            {
                words.Sort();
            }
        }

        public override string ToString()
        {
            string str = "============ Words List ============\n";
            foreach (string item in words)
            {
                 str += $"word[{words.IndexOf(item)}]: {item}\n";
            }
            return str;
        }

        
    }
}
