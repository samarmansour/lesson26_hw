﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson26_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            MyProtectedUniqueList list = new MyProtectedUniqueList("Draw");
            list.Add("Play");
            list.Add("Run");
            list.Add("Fly");
            list.Add("Drive");
            list.Add("Write");
            Console.WriteLine(list);

            try
            {
                list.Add("Run");
                list.Remove("Programming");
                list.RemoveAt(10);
                list.Sort(" ");
                list.Clear("");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.WriteLine("Done!!");
            }

            MyProtectedUniqueListGenerics<int> listGenerics = new MyProtectedUniqueListGenerics<int>("15246D");
            listGenerics.Add(10);
            listGenerics.Add(1);
            listGenerics.Add(20);
            listGenerics.Add(5);
            listGenerics.Add(0);

            try
            {
                listGenerics.Add(5);
                listGenerics.Remove(33);
                listGenerics.RemoveAt(8);
                listGenerics.Sort("");
                listGenerics.Clear(" ");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.WriteLine("Done!!");
            }

        }
    }
}
